#!/bin/bash

DEPLOY_DIR=$HOME/deploy/x64

rm -rf $DEPLOY_DIR
mkdir -p $DEPLOY_DIR

conan profile update settings.arch=x86_64 default
conan profile update settings.compiler.libcxx=libstdc++11 default
conan config set storage.download_cache="$HOME/conan/cache"


echo conan install:  zlib, libpng, boost, libpq, libxml2, openssl, libcurl, icu, xmlsec, libxslt, libjpeg-turbo, nss

conan install . --install-folder $DEPLOY_DIR \
     --settings arch=x86_64 \
     --settings compiler.libcxx=libstdc++11 \
     --build=automake --build=libtool \
     --build libiconv \
     --build zlib \
     --build libpng \
     --build boost \
     --build libpq \
     --build libxml2 \
     --build openssl \
     --build libcurl \
     --build icu \
     --build xmlsec \
     --build libxslt \
     --build libjpeg-turbo \
     --build nss


