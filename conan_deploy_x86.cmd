

set CONAN_HOME=c:\dev\conan

set DEPLOY_DIR=%CONAN_HOME%\deploy\x86

REM rm -rf $DEPLOY_DIR
mkdir -p %DEPLOY_DIR%

REM conan profile update settings.arch=x86 default
conan config set storage.download_cache="%CONAN_HOME%\cache"


echo conan install x86:  zlib, libpng, boost, libpq, libxml2, openssl, libcurl, icu, xmlsec, libxslt, libjpeg-turbo, nss

conan install . --install-folder %DEPLOY_DIR% ^
     --settings arch=x86 ^
     --settings compiler.runtime=MT ^
     --build libiconv ^
     --build expat ^
     --build zlib ^
     --build libpng ^
     --build boost ^
     --build libpq ^
     --build libxml2 ^
     --build openssl ^
     --build libcurl ^
     --build icu ^
     --build xmlsec ^
     --build libxslt ^
     --build libjpeg-turbo ^
     --build nss

