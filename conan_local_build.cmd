#!/bin/bash



set CONAN_HOME=c:\dev\conan

REM mkdir -p %CONAN_HOME%\cache


conan config set storage.download_cache="%CONAN_HOME%\cache"


cd conan-libiconv 
python build.py
cd ..

cd conan-xmlsec
python build.py
cd ..

cd conan-libxslt
python build.py
cd ..

cd conan-icu
python build.py
cd .. 

cd conan-mozilla-build
python build.py
cd ..

cd conan-nss
python build.py
cd ..

cd conan-libpng
python build.py
cd ..