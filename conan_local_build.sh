#!/bin/bash

conan config set storage.download_cache="$HOME/conan/cache"

cmake_install() {

if [ `which cmake` ]; then
    echo "cmake installed `cmake --version`"
else

    curl -LO https://cmake.org/files/v3.12/cmake-3.12.3.tar.gz
    tar zxvf cmake-3.12.3.tar.gz
    cd cmake-3.12.3
    ./bootstrap --prefix=/usr/local
    make -j$(nproc)
    sudo make install
fi

}

build_gitlab() {
  if [ "`conan search $2 | grep -c ^$2\/`" != "0" ]; then 
     echo "conan $2 postoji"
     return
  fi

  mkdir  -p $HOME/conan
  cd $HOME/conan
  echo ================ icu ===============================
  if [ ! -d $1 ] ; then
    git clone git@gitlab.com:hernad/$1.git 
  fi
  cd $1 && python3 build.py && cd ..
  if [ "$?" != "0" ] ; then
      echo "package $1 build error"
      exit 100
  fi
}

START_PWD=`pwd`
cmake_install


build_gitlab conan-libiconv libiconv
build_gitlab conan-xmlsec xmlsec
build_gitlab conan-libxslt libxslt
build_gitlab conan-icu icu
build_gitlab conan-nss nss
build_gitlab conan-libpng libpng


